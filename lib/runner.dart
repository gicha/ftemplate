import 'dart:async';

import 'package:flutter/widgets.dart';

import 'app/app.dart';
import 'enviroment/enviroment.dart';

/// Запуск приложения
Future<void> runner() async {
  WidgetsFlutterBinding.ensureInitialized();

  runZonedGuarded<Future<void>>(
    () async {
      await AppEnviroment().init();
      runApp(const App());
    },
    _onAppError,
  );
}

void _onAppError(exception, stack) {
  // здесь можно использовать Crashlytics или аналог
  // FirebaseCrashlytics.instance.recordError(exception, stack);
}
