part of 'auth_bloc.dart';

/// Base state for auth.
abstract class BaseAuthState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitAuthState extends BaseAuthState {}

class AuthLoadingState extends BaseAuthState {}

class ErrorAuthLoadingState extends BaseAuthState {}

class UnauthorizatedAuthState extends BaseAuthState {}

class LogginedAuthState extends BaseAuthState implements IUserAvailable {
  LogginedAuthState(this._user);

  final UserModel _user;

  @override
  List<Object> get props => [user];

  @override
  UserModel get user => _user;
}

abstract class IUserAvailable {
  /// Current user.
  UserModel get user;
}
