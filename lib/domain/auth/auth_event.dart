part of 'auth_bloc.dart';

/// Base event for auth.
class BaseAuthEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class AuthLoadEvent extends BaseAuthEvent {}

class LoginAuthEvent extends BaseAuthEvent {
  LoginAuthEvent({
    required this.login,
    required this.password,
  });

  final String login;
  final String password;

  @override
  List<Object?> get props => [login, password];
}

class LogoutAuthEvent extends BaseAuthEvent {}
