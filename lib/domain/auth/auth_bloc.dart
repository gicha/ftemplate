import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:ftemplate/data/sources/auth/local/auth_local_ds.dart';

import '../../data/models/models.dart';
import '../../data/sources/auth/remote/auth_remote_ds.dart';

part 'auth_event.dart';
part 'auth_state.dart';

/// Bloc for working with auth states.
class AuthBloc extends Bloc<BaseAuthEvent, BaseAuthState> {
  /// Create an instance [AuthBloc].
  AuthBloc(
    this._localDs,
    this._remoteDs,
  ) : super(InitAuthState()) {
    on<AuthLoadEvent>(_loadAuth);
    on<LoginAuthEvent>(_loginAuth);
    on<LogoutAuthEvent>(_logoutAuth);
  }

  final AuthRemoteDataSource _remoteDs;
  final AuthLocalDataSource _localDs;

  FutureOr<void> _loadAuth(
    AuthLoadEvent event,
    Emitter<BaseAuthState> emit,
  ) async {
    // TODO (you): https://github.com/Elementary-team/flutter-elementary/blob/main/examples/profile/lib/features/profile/service/profile_bloc/profile_bloc.dart
  }

  FutureOr<void> _loginAuth(
    LoginAuthEvent event,
    Emitter<BaseAuthState> emit,
  ) async {
    // TODO (you): made that
  }

  FutureOr<void> _logoutAuth(
    LogoutAuthEvent event,
    Emitter<BaseAuthState> emit,
  ) async {
    // TODO (you): made that
  }
}
