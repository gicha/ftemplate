import 'package:auto_route/auto_route.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/widgets.dart';

import '../ui/features/auth/auth_screen_widget.dart';
import '../ui/features/auth/auth_screen_wm.dart';
import '../ui/features/home/home_screen_widget.dart';
import '../ui/features/home/home_screen_wm.dart';
import 'guards/auth_guard.dart';

part 'app_router.gr.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'ScreenWidget,Route',
  routes: <AutoRoute>[
    AutoRoute(
      page: AuthScreenWidget,
      path: 'auth',
      children: [
        //all login screens
      ],
    ),
    AutoRoute(
      page: HomeScreenWidget,
      guards: [AuthGuard],
      initial: true,
      path: '/',
      children: <AutoRoute>[
        //all screens after login
      ],
    ),
  ],
)
class AppRouter extends _$AppRouter {
  AppRouter(AuthGuard authGuard) : super(authGuard: authGuard);
}
