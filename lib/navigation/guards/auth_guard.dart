import 'package:auto_route/auto_route.dart';
import 'package:ftemplate/domain/auth/auth_bloc.dart';

import '../app_router.dart';

class AuthGuard extends AutoRouteGuard {
  AuthGuard(this.authBloc);
  AuthBloc authBloc;

  @override
  Future<void> onNavigation(
    NavigationResolver resolver,
    StackRouter router,
  ) async {
    if (authBloc.state is IUserAvailable) {
      resolver.next();
    } else {
      await router.push(AuthRoute());
      if (authBloc.state is IUserAvailable) {
        resolver.next();
      } else {
        router.navigate(resolver.route.toPageRouteInfo());
      }
    }
  }
}
