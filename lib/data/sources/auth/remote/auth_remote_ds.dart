import 'dart:async';

import 'package:chopper/chopper.dart';

import '../../../models/models.dart';

part 'auth_remote_ds.chopper.dart';

@ChopperApi(baseUrl: '/auth/')
abstract class AuthRemoteDataSource extends ChopperService {
  static AuthRemoteDataSource create([ChopperClient? client]) => _$AuthRemoteDataSource(client);

  @Post(path: 'login')
  Future<Response<AuthSession>> login(@Body() AuthRequest body);

  @Post(path: 'signup')
  Future<Response<AuthSession>> signup(@Body() AuthRequest body);

  @Post(path: 'forget')
  Future<Response<Map<String, dynamic>>> forget(@Body() AuthForgetRequest body);
}
