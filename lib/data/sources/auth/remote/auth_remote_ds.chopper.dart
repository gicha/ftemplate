// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_remote_ds.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations, unnecessary_brace_in_string_interps
class _$AuthRemoteDataSource extends AuthRemoteDataSource {
  _$AuthRemoteDataSource([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AuthRemoteDataSource;

  @override
  Future<Response<AuthSession>> login(AuthRequest body) {
    final $url = '/auth/login';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<AuthSession, AuthSession>($request);
  }

  @override
  Future<Response<AuthSession>> signup(AuthRequest body) {
    final $url = '/auth/signup';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<AuthSession, AuthSession>($request);
  }

  @override
  Future<Response<Map<String, dynamic>>> forget(AuthForgetRequest body) {
    final $url = '/auth/forget';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<Map<String, dynamic>, Map<String, dynamic>>($request);
  }
}
