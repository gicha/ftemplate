import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../models/models.dart';

class AuthLocalDataSource extends ChangeNotifier {
  AuthLocalDataSource() {
    _storage.read(key: _sessionKey).then(onInit);
  }

  static const _sessionKey = 'persistSession';
  final _storage = const FlutterSecureStorage();

  Completer authed = Completer();
  AuthSession? session;

  Future<void> onInit(String? storedSession) async {
    if (storedSession == null) {
      authed.complete();
      return;
    }
    session = Mapper.fromJson(storedSession);

    notifyListeners();
    authed.complete();
  }

  void storeSession(AuthSession session) {
    this.session = session;

    notifyListeners();
    _storage.write(
      key: _sessionKey,
      value: session.toJson(),
    );
  }

  Future<void> logout() async {
    session = null;
    await _storage.deleteAll();
    authed = Completer();
    notifyListeners();
  }
}
