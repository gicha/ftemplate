import '../models.dart';

class UserModel with Mappable {
  UserModel({
    required this.id,
    required this.name,
    this.surname,
  });

  final String id;
  final String name;
  final String? surname;
}
