import '../models.dart';

class AuthForgetRequest with Mappable {
  AuthForgetRequest(this.email);
  final String email;
}

class AuthRestoreRequest with Mappable {
  AuthRestoreRequest(
    this.restoreToken,
    this.password,
  );

  final String restoreToken;
  final String password;
}

class AuthRequest with Mappable {
  AuthRequest(
    this.email,
    this.password,
  );

  final String email;
  final String password;
}

class AuthSession with Mappable {
  AuthSession(this.token);

  final String token;
}
