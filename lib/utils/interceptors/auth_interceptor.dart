import 'package:chopper/chopper.dart';

import '../../data/sources/auth/local/auth_local_ds.dart';

class AuthInterceptor implements RequestInterceptor {
  const AuthInterceptor(this.localAuth);
  final AuthLocalDataSource localAuth;

  @override
  Future<Request> onRequest(Request request) async {
    final token = localAuth.session?.token;
    return applyHeaders(
      request,
      {if (token != null) 'Authorization': 'Bearer $token'},
    );
  }
}
