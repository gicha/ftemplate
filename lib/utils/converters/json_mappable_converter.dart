import 'package:chopper/chopper.dart';

import '../../data/models/models.dart';

class JsonMappableConverter extends JsonConverter {
  @override
  Response<ResultType> convertResponse<ResultType, Item>(Response response) {
    final jsonRes = super.convertResponse(response);
    return jsonRes.copyWith<ResultType>(body: Mapper.fromValue(jsonRes.body));
  }

  @override
  Request convertRequest(Request request) {
    return super.convertRequest(
      request.copyWith(body: Mapper.toValue(request.body)),
    );
  }
}
