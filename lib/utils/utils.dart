import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

export 'logger.dart';

class Utils {
  static String getNoun(int number, String one, String two, String five) {
    int n = number.abs() % 100;
    if (n >= 5 && n <= 20) {
      return five;
    }
    n %= 10;
    if (n == 1) {
      return one;
    }
    if (n >= 2 && n <= 4) {
      return two;
    }
    return five;
  }

  static String numToFixStr(num number, [int maxFractionDigits = 2]) {
    String str = '';
    if (number > 0 && number < 1) {
      str = number.toStringAsFixed(maxFractionDigits);
    } else {
      str = number
          .toStringAsFixed(maxFractionDigits)
          .replaceAll('0', ' ')
          .trim() //remove .X00
          .replaceAll(' ', '0')
          .replaceAll('.', ' ')
          .trim() //remove point in the end
          .replaceAll(' ', '.');
    }
    if (str.isEmpty) return '0';
    return str;
  }

  static bool isSameDate(DateTime a, DateTime b) {
    return a.day == b.day && a.month == b.month && a.year == b.year;
  }

  static bool isToday(DateTime dateTime) {
    return isSameDate(DateTime.now(), dateTime);
  }

  static String twoDigits(int value) {
    return value.toString().length == 1 ? '0$value' : value.toString();
  }

  static String fromToTime(DateTime t1, DateTime t2) {
    DateTime a = t1.toLocal();
    DateTime b = t2.toLocal();
    if (isSameDate(a, b)) {
      String day = isToday(a) ? 'Сегодня' : Jiffy(a).MEd;
      return '$day, ${a.hour}:${twoDigits(a.minute)} — ${a.hour}:${twoDigits(a.minute)}';
    } else {
      return '${Jiffy(a).MMMMEEEEd}, ${a.hour}:${twoDigits(a.minute)} — ${Jiffy(a).MMMMEEEEd}, ${a.hour}:${twoDigits(a.minute)}';
    }
  }

  static String distanceFromMeters(double meters) {
    if (meters >= 1000) return '${(meters / 100).toStringAsFixed(1)} км';
    return '${meters.round()} м';
  }

  static String durationFromSeconds(double seconds) {
    if (seconds >= 3600) return '${(seconds / 3600).toStringAsFixed(1)} час';
    if (seconds >= 60) return '${(seconds / 60).round()} мин';
    return '${seconds.round()} сек';
  }

  static DateTime substratMonthes(DateTime dateTime, int monthes) {
    int years = dateTime.year - (monthes / 12).floor();
    int month = (monthes % 12).floor();
    if (dateTime.month < month) {
      years--;
      month = 12 + dateTime.month - month;
    } else {
      month = dateTime.month - month;
    }
    return DateTime(
      years,
      month,
      dateTime.day,
      dateTime.hour,
      dateTime.minute,
      dateTime.millisecond,
      dateTime.microsecond,
    );
  }

  static String validateEmail(String? value) {
    String pattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    RegExp regex = RegExp(pattern);
    if (value != null && value.isNotEmpty && !regex.hasMatch(value)) {
      return 'Enter valid email address';
    } else {
      return '';
    }
  }

  static String validateCardNumber(String? value) {
    String pattern =
        r'(^4[0-9]{12}(?:[0-9]{3})?$)|(^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$)|(3[47][0-9]{13})|(^3(?:0[0-5]|[68][0-9])[0-9]{11}$)|(^6(?:011|5[0-9]{2})[0-9]{12}$)|(^(?:2131|1800|35\d{3})\d{11}$)';
    RegExp regex = RegExp(pattern);
    if (value != null && value.isNotEmpty && !regex.hasMatch(value.replaceAll(' ', ''))) {
      return 'Enter valid card number';
    } else {
      return '';
    }
  }

  static String validateCardHolder(String? value) {
    String pattern = '[a-zA-Z]+ [a-zA-Z]+';
    RegExp regex = RegExp(pattern);
    if (value != null && value.isNotEmpty && !regex.hasMatch(value)) {
      return 'Enter valid card holder';
    } else {
      return '';
    }
  }

  static String validateExpirationDate(String? value) {
    String pattern = '[0-1][0-9]/[0-9][0-9]';
    RegExp regex = RegExp(pattern);
    if (value != null && value.isNotEmpty && !regex.hasMatch(value)) {
      return 'Enter valid card expiration';
    } else {
      return '';
    }
  }

  static String validateCvv(String? value) {
    String pattern = '[0-9][0-9][0-9]';
    RegExp regex = RegExp(pattern);
    if (value != null && value.isNotEmpty && !regex.hasMatch(value)) {
      return 'Enter valid cvv';
    } else {
      return '';
    }
  }

  static String getShortCardNumber(String cardNumber) {
    String lastNums = cardNumber.substring(cardNumber.length - 4, cardNumber.length);
    return '** $lastNums';
  }

  static String getInviteCodeExpirationDate(DateTime date) {
    return DateFormat('dd.MM.yy hh:mm').format(date);
  }

  static DateTime convertStringsToDateTime(String day, String time) {
    var tempTime = time.split(' ').first;
    return DateTime.utc(
      int.parse(day.split('.')[0]),
      int.parse(day.split('.')[1]),
      int.parse(day.split('.')[2]),
      int.parse(tempTime.split(':')[0]),
      int.parse(tempTime.split(':')[1]),
    );
  }
}
