import 'dart:developer';

import 'package:elementary/elementary.dart';

/// Implementation of ErrorHandler
/// prints error to console in logs
class LogErrorHandler implements ErrorHandler {
  @override
  void handleError(Object error) {
    log(error.toString());
  }
}