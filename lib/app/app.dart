import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:ftemplate/di/scopes/global/abstract_global_scope.dart';

import '../di/common/di_scope_widget.dart';
import '../di/scopes/global/global_scope.dart';
import '../generated/l10n.dart';
import '../ui/theme/theme.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  late IGlobalScope _scope;

  @override
  void initState() {
    super.initState();
    _rebuildChildTree();
  }

  Future<void> _rebuildChildTree() async {
    setState(() {
      _scope = GlobalScope(_rebuildChildTree);
    });
  }

  @override
  Widget build(BuildContext context) {
    return DiScope<IGlobalScope>(
      key: ObjectKey(_scope),
      factory: () => _scope,
      // child: EntityStateNotifierBuilder( – если мониторим какую-то глобальную настройку для App, например themeMode: ThemeMode.light / dark
      child: Builder(
        builder: (_) {
          if (Platform.isAndroid) {
            return MaterialApp.router(
              title: _scope.envModel.appName,
              debugShowCheckedModeBanner: false,
              //localization
              // locale: context.watch<LocaleProvider>().locale,
              supportedLocales: AppString.delegate.supportedLocales,
              localizationsDelegates: const [
                AppString.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate
              ],
              //theming
              theme: AppTheme.theme,
              //navigation
              routerDelegate: _scope.router.delegate(),
              routeInformationParser: _scope.router.defaultRouteParser(),
              //builders
              builder: (context, child) {
                return child ?? const SizedBox();
              },
            );
          } else {
            return CupertinoApp.router(
              title: _scope.envModel.appName,
              debugShowCheckedModeBanner: false,
              //localization
              // locale: context.watch<LocaleProvider>().locale,
              supportedLocales: AppString.delegate.supportedLocales,
              localizationsDelegates: const [
                AppString.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate
              ],
              //theming
              theme: MaterialBasedCupertinoThemeData(
                materialTheme: AppTheme.theme,
              ),
              //navigation
              routerDelegate: _scope.router.delegate(),
              routeInformationParser: _scope.router.defaultRouteParser(),
              //builders
              builder: (context, child) {
                return child ?? const SizedBox();
              },
            );
          }
        },
      ),
    );
  }
}
