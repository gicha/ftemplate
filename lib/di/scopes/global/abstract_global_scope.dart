import 'package:chopper/chopper.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';
import 'package:ftemplate/data/models/local/env_model.dart';

import '../../../data/sources/auth/local/auth_local_ds.dart';
import '../../../data/sources/auth/remote/auth_remote_ds.dart';
import '../../../domain/auth/auth_bloc.dart';
import '../../../navigation/app_router.dart';

/// Global dependencies.
abstract class IGlobalScope {
  /// обработка ошибок бизнес логики
  ErrorHandler get errorHandler;

  /// запускатель перестройки всего поддерева
  VoidCallback get treeRebuilder;

  /// env
  EnvModel get envModel;

  /// http
  ChopperClient get apiClient;

  /// навигация приложения
  AppRouter get router;

  /// источники данных для авторизации
  AuthRemoteDataSource get authRemoteDataSource;
  AuthLocalDataSource get authLocalDataSource;

  /// блок аутентификации
  AuthBloc get authBloc;
}
