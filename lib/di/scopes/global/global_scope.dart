import 'package:chopper/chopper.dart';
import 'package:elementary/elementary.dart';
import 'package:flutter/foundation.dart';
import 'package:ftemplate/data/models/local/env_model.dart';
import 'package:ftemplate/data/sources/auth/remote/auth_remote_ds.dart';
import 'package:ftemplate/domain/auth/auth_bloc.dart';
import 'package:ftemplate/navigation/app_router.dart';
import 'package:ftemplate/navigation/guards/auth_guard.dart';
import '../../../data/sources/auth/local/auth_local_ds.dart';
import '../../../enviroment/enviroment.dart';
import '../../../utils/converters/json_mappable_converter.dart';
import '../../../utils/handlers/log_error_handler.dart';
import '../../../utils/interceptors/auth_interceptor.dart';
import 'abstract_global_scope.dart';

/// Глобальные зависимости приложения
class GlobalScope implements IGlobalScope {
  /// Create an instance [DevGlobalScope].
  GlobalScope(VoidCallback treeRebuilder) {
    _treeRebuilder = treeRebuilder;
    _errorHandler = LogErrorHandler();
    _appEnviroment = AppEnviroment();
    _authLocalDataSource = AuthLocalDataSource();
    _apiClient = ChopperClient(
      baseUrl: envModel.apiUrl,
      converter: JsonMappableConverter(),
      interceptors: [
        AuthInterceptor(authLocalDataSource),
        HttpLoggingInterceptor(),
      ],
    );
    _authRemoteDataSource = AuthRemoteDataSource.create(apiClient);
    _authBloc = AuthBloc(
      authLocalDataSource,
      authRemoteDataSource,
    );
    _router = AppRouter(AuthGuard(authBloc));
  }

  late final VoidCallback _treeRebuilder;
  late final ErrorHandler _errorHandler;
  late final AppEnviroment _appEnviroment;
  late final AuthLocalDataSource _authLocalDataSource;
  late final ChopperClient _apiClient;
  late final AuthRemoteDataSource _authRemoteDataSource;
  late final AuthBloc _authBloc;
  late final AppRouter _router;

  @override
  VoidCallback get treeRebuilder => _treeRebuilder;

  @override
  ErrorHandler get errorHandler => _errorHandler;

  @override
  EnvModel get envModel => _appEnviroment.envModel;

  @override
  ChopperClient get apiClient => _apiClient;

  @override
  AuthLocalDataSource get authLocalDataSource => _authLocalDataSource;

  @override
  AuthRemoteDataSource get authRemoteDataSource => _authRemoteDataSource;

  @override
  AuthBloc get authBloc => _authBloc;

  @override
  AppRouter get router => _router;
}
