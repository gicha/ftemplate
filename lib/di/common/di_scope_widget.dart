import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

typedef ScopeFactory<T> = T Function();

/// контейнер для инъекции зависимостей нужного типа T (например [AppScope]).
class DiScope<T> extends StatefulWidget {
  const DiScope({
    required this.factory,
    required this.child,
    Key? key,
  }) : super(key: key);

  /// Factory , которая возвращает область зависимости
  final ScopeFactory<T> factory;

  /// Оборачиваемый виджет
  final Widget child;

  @override
  _DiScopeState createState() => _DiScopeState<T>();
}

/// стейт для DiScope<T>
class _DiScopeState<T> extends State<DiScope<T>> {
  late T scope;

  @override
  void initState() {
    super.initState();
    scope = widget.factory();
  }

  @override
  Widget build(BuildContext context) {
    return Provider<T>(
      create: (_) => scope,
      child: widget.child,
    );
  }
}
