import 'dart:async';

import 'package:flutter_dotenv/flutter_dotenv.dart';

import '../data/models/models.dart';
import '../utils/utils.dart';

class AppEnviroment {
  factory AppEnviroment() {
    return _instance;
  }
  AppEnviroment._create();

  static final AppEnviroment _instance = AppEnviroment._create();

  EnvType get defaultEnv => EnvType.devel;

  EnvType? currentEnvType;
  late EnvModel envModel;

  final _logger = getLogger('AppEnviroment');

  Completer<EnvModel> inited = Completer();

  Future<void> init() async {
    EnvType envType = String.fromEnvironment(
      'env',
      defaultValue: defaultEnv.name,
    ).toLowerCase().toEnvType;
    await dotenv.load(fileName: 'env/${envType.name}.env');
    envModel = Mapper.fromMap(dotenv.env);
    inited.complete(envModel);
    _logger.i('APP WILL USE ${envType.name.toUpperCase()} ENVIROMENT');
  }
}
