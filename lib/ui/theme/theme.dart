library app_theme;

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

part 'border_radius/app_border_radius.dart';
part 'app_theme.dart';

part 'colors/app_colors.dart';
part 'colors/background.dart';
part 'colors/main.dart';
part 'colors/stroke.dart';
part 'colors/text.dart';

part 'gradients/gradients.dart';

part 'typography/app_text_styles.dart';
part 'typography/gfont.dart';
part 'typography/header.dart';
part 'typography/subtitle.dart';
part 'typography/body.dart';
part 'typography/button.dart';
part 'typography/caption.dart';
part 'typography/extension.dart';
