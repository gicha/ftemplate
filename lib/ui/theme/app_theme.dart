part of 'theme.dart';

class AppTheme {
  static ThemeData theme = ThemeData.from(
    textTheme: AppTextStyles.textTheme,
    colorScheme: ColorScheme(
      brightness: Brightness.light,
      background: AppColors.bg.primary,
      error: AppColors.text.red,
      primary: AppColors.main.primary,
      secondary: AppColors.main.secondary,
      tertiary: AppColors.text.tertiary,
      onSecondary: AppColors.bg.primary,
      onError: AppColors.bg.primary,
      surface: AppColors.bg.secondary,
      onPrimary: AppColors.bg.primary,
      onSurface: AppColors.main.primary,
      onBackground: AppColors.main.primary,
    ),
  );
}
