part of '../theme.dart';

class AppGradients {
  static final primary = LinearGradient(
    colors: [
      AppColors.main.primary,
      AppColors.bg.primary,
    ],
  );
}
