part of '../theme.dart';

class BackgroundColors {
  // ********** BACKGROUND ***********

  /// Основной фон
  /// #FFFFFF
  final primary = const Color(0xFFFFFFFF);

  /// Вторичный фон
  /// #EFEFEF
  final secondary = const Color(0xFFEFEFEF);

  /// Элементы цвета Secondary при нажатии
  /// #FFFFFF
  final block = const Color(0xFFFFFFFF);

  /// Фон в уведомлениях об успешном действии
  /// #E1FFE0
  final success = const Color(0xFFE1FFE0);

  /// Фон в уведомлениях об ошибке
  /// #FFEAEA
  final danger = const Color(0xFFFFEAEA);

  /// Фон в уведомлениях с предупреждением
  /// #FFF5E6
  final warning = const Color(0xFFFFF5E6);

  /// Фон в информативных уведомлениях
  /// #DDEBFC
  final info = const Color(0xFFDDEBFC);
}
