part of '../theme.dart';

class AppColors {
  static final main = MainColors();
  static final bg = BackgroundColors();
  static final text = TextColors();
  static final stroke = StrokeColors();
}
