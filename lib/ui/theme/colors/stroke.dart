part of '../theme.dart';

class StrokeColors {
  // ********** STROKE ***********

  /// Активное состояние поля ввода
  /// #151515
  final activeBlack = const Color(0xFF151515);

  /// Первичный цвет иконок, а также
  /// контроллов в состоянии Selected
  /// #321946
  final iconPrimary = const Color(0xFF321946);

  /// Вторичный цвет иконок
  /// #CBCBCB
  final iconSecondary = const Color(0xFFCBCBCB);

  /// Неактивное состояние поля ввода, divider
  /// #EFEFEF
  final inactiveGrey = const Color(0xFFEFEFEF);

  /// Цвет checkbox, radiobutton, toggle switch и т.д. в состоянии Not selected, иконки в неактивных элементах
  /// #B9B9B9
  final tertiaryLight = const Color(0xFFB9B9B9);

  /// Ошибки
  /// #EB4A4A
  final danger = const Color(0xFFEB4A4A);

  /// Ошибки при нажатии
  /// #D12020
  final dangerPress = const Color(0xFFD12020);

  /// Иконки цвета на фоне цвета Main Primary
  /// #FFFFFF
  final iconWhite = const Color(0xFFFFFFFF);

  /// Иконки предупреждения в уведомлениях
  /// #FFAF37
  final iconWarning = const Color(0xFFFFAF37);

  /// Иконки ошибки в уведомлениях
  /// #EB4A4A
  final iconError = const Color(0xFFEB4A4A);

  /// Иконки успешного выполнения в уведомлениях
  /// #279761
  final iconSuccess = const Color(0xFF279761);

  /// Иконки в информативных уведомлениях
  /// #2A8BFE
  final iconInfo = const Color(0xFF2A8BFE);
}
