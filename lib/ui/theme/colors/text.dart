part of '../theme.dart';

class TextColors {
  // ********** TEXT ***********

  /// Основной текст
  /// #151515
  final primary = const Color(0xFF151515);

  /// Текст на вторичных кнопках (Filled, Outlined, Ghost)
  /// #240B39
  final secondary = const Color(0xFF240B39);

  /// Caption (названия полей ввода, подписи), пояснения
  /// #818181
  final tertiary = const Color(0xFF818181);

  /// Placeholder в полях ввода текст в элементах цвета Main Primary, Main Secondary (состояние Disable)
  /// #B9B9B9
  final tertiaryLight = const Color(0xFFB9B9B9);

  /// Для надписей в элементах цвета Main Primary и Main Accent (во всех состояниях), а также в элементах цвета Secondary в состоянии Disable
  /// #FFFFFF
  final staticWhite = const Color(0xFFFFFFFF);

  /// Ошибки, отрицательные действия
  /// #EB4A4A
  final red = const Color(0xFFEB4A4A);

  /// Пояснение в поле ввода цены на экране заказа
  /// #F3A000
  final accent = const Color(0xFFF3A000);
}
