part of '../theme.dart';

class MainColors {
  // ********** MAIN ***********

  /// Основной цвет элементов
  /// #321946
  final primary = const Color(0xFF321946);

  /// Элементы цвета Primary при нажатии
  /// #1D092E
  final primaryPress = const Color(0xFF1D092E);

  /// Заблокированные элементы цвета Primary
  /// #AC9FB7
  final primaryDisable = const Color(0xFFAC9FB7);

  /// Осветленный оттенок цвета Primary
  /// #8968A4
  final primaryLight = const Color(0xFF8968A4);

  /// Вторичный цвет элементов
  /// #EFEFEF
  final secondary = const Color(0xFFEFEFEF);

  /// Элементы цвета Secondary при нажатии
  /// #C9C9C9
  final secondaryPress = const Color(0xFFC9C9C9);

  /// Заблокированные элементы цвета Secondary
  /// #EAEAEA
  final secondaryDisable = const Color(0xFFEAEAEA);

  /// Темный оттенок цвета Secondary
  /// #CBCBCB
  final secondaryDark = const Color(0xFFCBCBCB);

  /// Акцентный цвет
  /// #F3A000
  final accent = const Color(0xFFF3A000);

  /// Элементы акцентного цвета при нажатии
  /// #EC9006
  final accentPress = const Color(0xFFEC9006);

  /// Заблокированные элементы акцентного цвета
  /// #FFD481
  final accentDisable = const Color(0xFFFFD481);

  /// Осветленный оттенок акцентного цвета
  /// #FFC14A
  final accentLight = const Color(0xFFFFC14A);
}
