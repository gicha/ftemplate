part of '../theme.dart';

class BodyTextStyles {
  /// Body L SemiBold
  final b1Semi = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 16,
    figmaHeight: 24,
  );

  /// Body L Regular
  final b1 = GFont.create(
    fontSize: 16,
    figmaHeight: 24,
  );

  /// Body S SemiBold
  final b2Semi = GFont.create(
    fontWeight: FontWeight.w600,
    figmaHeight: 20,
  );

  /// Body S Regular
  final b2 = GFont.create(
    figmaHeight: 20,
  );

  /// Body XS Regular
  final b3 = GFont.create(
    figmaHeight: 16,
  );
}
