part of '../theme.dart';

class CaptionTextStyles {
  /// Caption
  final caption = GFont.create(
    fontSize: 12,
    figmaHeight: 16,
  );
}
