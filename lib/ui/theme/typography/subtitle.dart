part of '../theme.dart';

class SubtitleTextStyles {
  /// Subtitle1 18/24px, Semibold
  final subtitle1 = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 18,
    figmaHeight: 24,
  );

  /// Subtitle2 18/24px, Regular
  final subtitle2 = GFont.create(
    fontSize: 18,
    figmaHeight: 24,
  );
}
