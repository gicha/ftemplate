part of '../theme.dart';

extension TextExtension on TextStyle {
  TextStyle get bg => copyWith(color: AppColors.bg.primary);
  TextStyle get primary => copyWith(color: AppColors.main.primary);
  TextStyle get accent => copyWith(color: AppColors.main.accent);
  TextStyle get tertiary => copyWith(color: AppColors.text.tertiary);
  TextStyle get tertiaryLight => copyWith(color: AppColors.text.tertiaryLight);
  TextStyle get red => copyWith(color: AppColors.text.red);
  TextStyle get bgSecondary => copyWith(color: AppColors.bg.secondary);
  TextStyle get textSecondary => copyWith(color: AppColors.text.secondary);
  TextStyle get medium => copyWith(fontWeight: FontWeight.w500);
  TextStyle get semiBold => copyWith(fontWeight: FontWeight.w600);
  TextStyle get bold => copyWith(fontWeight: FontWeight.bold);
  TextStyle get underline => copyWith(decoration: TextDecoration.underline);
}
