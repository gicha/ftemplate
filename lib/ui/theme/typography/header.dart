part of '../theme.dart';

class HeaderTextStyles {
  /// Headline 1
  final h1 = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 32,
    figmaHeight: 40,
  );

  /// Headline 2
  final h2 = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 28,
    figmaHeight: 36,
  );

  /// Headline 3
  final h3 = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 22,
    figmaHeight: 28,
  );

  /// Subheader 1
  final h4 = GFont.create(
    fontWeight: FontWeight.w600,
    fontSize: 18,
    figmaHeight: 24,
  );

  /// Subheader 2
  final h5 = GFont.create(
    fontSize: 18,
    figmaHeight: 24,
  );
}
