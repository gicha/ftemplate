part of '../theme.dart';

class ButtonTextStyles {
  /// Button M
  final m = GFont.create(
    fontWeight: FontWeight.w500,
    fontSize: 16,
    figmaHeight: 24,
  );

  /// Button S
  final s = GFont.create(
    fontWeight: FontWeight.w500,
    figmaHeight: 20,
  );
}
