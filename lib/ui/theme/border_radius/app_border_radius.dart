part of '../theme.dart';

class AppBorderRadius {
  static const _radius = Radius.circular(4);
  static const _halfRadius = Radius.circular(2);

  static const button = BorderRadius.all(_radius);
  static const card = BorderRadius.all(_radius);
  static const smallCard = BorderRadius.all(_halfRadius);
  static const otherMessage = BorderRadius.only(
    bottomRight: _radius,
    topLeft: _radius,
    topRight: _radius,
  );
  static const selfMessage = BorderRadius.only(
    bottomLeft: _radius,
    topLeft: _radius,
    topRight: _radius,
  );
}
