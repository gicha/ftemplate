import 'package:elementary/elementary.dart';
import 'package:flutter/cupertino.dart';
import '../../../utils/handlers/log_error_handler.dart';
import 'home_screen_model.dart';
import 'home_screen_widget.dart';

abstract class IHomeScreenWidgetModel extends IWidgetModel {}

HomeScreenWidgetModel defaultHomeScreenWidgetModelFactory(
  BuildContext context,
) {
  // подтягивание глобальных зависимостей
  // final globalScope = context.read<IGlobalScope>();
  return HomeScreenWidgetModel(
    HomeScreenModel(LogErrorHandler()),
  );
}

class HomeScreenWidgetModel extends WidgetModel<HomeScreenWidget, HomeScreenModel> implements IHomeScreenWidgetModel {
  HomeScreenWidgetModel(HomeScreenModel model) : super(model);
}
