import 'package:elementary/elementary.dart';

/// Default Elementary model for HomeScreen module
class HomeScreenModel extends ElementaryModel {
  HomeScreenModel(ErrorHandler errorHandler) : super(errorHandler: errorHandler);
}
