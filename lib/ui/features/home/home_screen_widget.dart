import 'package:elementary/elementary.dart';
import 'package:flutter/material.dart';
import 'home_screen_wm.dart';

/// Main widget for HomeScreen module
class HomeScreenWidget extends ElementaryWidget<IHomeScreenWidgetModel> {
  const HomeScreenWidget({
    Key? key,
    WidgetModelFactory wmFactory = defaultHomeScreenWidgetModelFactory,
  }) : super(wmFactory, key: key);

  @override
  Widget build(IHomeScreenWidgetModel wm) {
    return const Scaffold(
      body: Center(
        child: Text('home'),
      ),
    );
  }
}
