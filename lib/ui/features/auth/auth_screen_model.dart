import 'package:elementary/elementary.dart';
import 'package:ftemplate/domain/auth/auth_bloc.dart';

/// Default Elementary model for AuthScreen module
class AuthScreenModel extends ElementaryModel {
  AuthScreenModel(ErrorHandler errorHandler, this.authBloc) : super(errorHandler: errorHandler);

  final AuthBloc authBloc;
}
