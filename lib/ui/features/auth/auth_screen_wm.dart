import 'package:elementary/elementary.dart';
import 'package:flutter/cupertino.dart';
import 'package:ftemplate/domain/auth/auth_bloc.dart';
import 'package:provider/provider.dart';
import '../../../di/scopes/global/abstract_global_scope.dart';
import '../../../utils/handlers/log_error_handler.dart';
import 'auth_screen_model.dart';
import 'auth_screen_widget.dart';

abstract class IAuthScreenWidgetModel extends IWidgetModel {}

AuthScreenWidgetModel defaultAuthScreenWidgetModelFactory(
  BuildContext context,
) {
  // подтягивание глобальных зависимостей
  final globalScope = context.read<IGlobalScope>();
  return AuthScreenWidgetModel(
    AuthScreenModel(
      LogErrorHandler(),
      globalScope.authBloc,
    ),
  );
}

class AuthScreenWidgetModel extends WidgetModel<AuthScreenWidget, AuthScreenModel> implements IAuthScreenWidgetModel {
  AuthScreenWidgetModel(AuthScreenModel model) : super(model);

  Future<void> login() async {
    model.authBloc.add(
      LoginAuthEvent(
        login: 'login',
        password: 'password',
      ),
    );
  }
}
