import 'package:flutter/material.dart';
import 'package:text_controller/text_controller.dart';

import '../../theme/theme.dart';

class AppSearchTextField extends StatefulWidget {
  const AppSearchTextField({Key? key, required this.controller}) : super(key: key);

  final AppTextController controller;

  @override
  _AppSearchTextFieldState createState() => _AppSearchTextFieldState();
}

class _AppSearchTextFieldState extends State<AppSearchTextField> {
  AppTextController get controller => widget.controller;

  final InputBorder border = InputBorder.none;

  @override
  void initState() {
    controller.addListener(() {
      if (mounted) setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: controller.focusNode,
      inputFormatters: [
        if (controller.mask != null) controller.maskFormatter,
      ],
      enabled: controller.enabled,
      style: AppTextStyles.body.b1,
      obscureText: controller.obscure,
      onEditingComplete: FocusScope.of(context).unfocus,
      decoration: InputDecoration(
        filled: false,
        counterText: '',
        contentPadding: const EdgeInsets.symmetric(vertical: 17),
        prefixIconConstraints: BoxConstraints.tight(const Size(24, 18)),
        prefixIcon: const Icon(
          Icons.search,
          size: 18,
          color: Color(0xFFBFBFBF),
        ),
        labelText: controller.label,
        hintText: controller.hintText,
        hintStyle: AppTextStyles.body.b1.tertiary,
        labelStyle: AppTextStyles.body.b1.tertiary,
        border: border,
        errorBorder: border,
        enabledBorder: border,
        focusedBorder: border,
        focusedErrorBorder: border,
        disabledBorder: border,
      ),
    );
  }
}
