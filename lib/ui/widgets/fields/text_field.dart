import 'package:flutter/material.dart';
import 'package:text_controller/text_controller.dart';

import '../../theme/theme.dart';

class AppTextField extends StatefulWidget {
  const AppTextField({
    Key? key,
    required this.controller,
    this.nextFocusNode,
    this.keyboardType = TextInputType.text,
    TextInputAction? textInputAction,
    this.maxLines = 1,
    this.textCapitalization = TextCapitalization.none,
    this.withClearButton = false,
    this.onTap,
    this.readOnly = false,
    this.maxLength,
    this.additionalText,
    this.obscureIconVisible,
    this.obscureIconInvisible,
  })  : textInputAction = textInputAction ?? (nextFocusNode != null ? TextInputAction.next : textInputAction),
        super(key: key);

  final AppTextController controller;
  final TextInputType keyboardType;
  final TextInputAction? textInputAction;
  final int? maxLines;
  final TextCapitalization textCapitalization;
  final bool withClearButton;
  final FocusNode? nextFocusNode;
  final VoidCallback? onTap;
  final bool readOnly;
  final Widget? obscureIconVisible;
  final Widget? obscureIconInvisible;
  final int? maxLength;
  final String? additionalText;

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  AppTextController get controller => widget.controller;

  TextInputType get keyboardType => widget.keyboardType;

  TextInputAction? get inputAction => widget.textInputAction;

  int? get maxLines => widget.maxLines;

  bool get hasError => error?.isNotEmpty ?? false;
  String? Function(String? v) get validator => controller.validator == null ? (_) => null : controller.validator!;

  String? get error => controller.error ?? validator(controller.text);

  final UnderlineInputBorder border = UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColors.stroke.tertiaryLight,
      width: 0.5,
    ),
  );
  final UnderlineInputBorder focusedBorder = UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColors.main.primary,
      width: 2,
    ),
  );

  @override
  void initState() {
    controller.addListener(() {
      if (mounted) setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          maxLength: widget.maxLength,
          controller: controller,
          focusNode: controller.focusNode,
          inputFormatters: [
            if (controller.mask != null) controller.maskFormatter,
          ],
          enabled: controller.enabled,
          keyboardType: keyboardType,
          readOnly: widget.readOnly,
          maxLines: maxLines,
          style: AppTextStyles.body.b1,
          textCapitalization: widget.textCapitalization,
          textInputAction: inputAction,
          onTap: widget.onTap,
          obscureText: controller.obscure,
          onEditingComplete: () {
            if (widget.nextFocusNode == null) {
              FocusScope.of(context).unfocus();
            } else {
              FocusScope.of(context).requestFocus(widget.nextFocusNode);
            }
          },
          decoration: InputDecoration(
            fillColor: AppColors.bg.primary,
            filled: true,
            counterText: '',
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 14,
            ),
            labelText: controller.label,
            hintText: controller.hintText,
            hintStyle: AppTextStyles.body.b1.tertiary,
            labelStyle: AppTextStyles.body.b1.tertiary,
            border: border,
            errorBorder: border,
            enabledBorder: border,
            focusedBorder: focusedBorder,
            focusedErrorBorder: border,
            disabledBorder: border,
            suffixIcon: Builder(
              builder: (BuildContext context) {
                if (widget.withClearButton) {
                  return GestureDetector(
                    onTap: controller.clear,
                    child: const Icon(Icons.clear),
                  );
                } else if (controller.withObscure) {
                  return GestureDetector(
                    onTap: () => setState(
                      () => controller.setObscure(!controller.obscure),
                    ),
                    child: (widget.obscureIconVisible == null || widget.obscureIconInvisible == null)
                        ? Icon(
                            controller.obscure ? Icons.visibility_off : Icons.visibility,
                          )
                        : Container(
                            padding: const EdgeInsets.all(10),
                            child: controller.obscure ? widget.obscureIconInvisible : widget.obscureIconVisible,
                          ),
                  );
                }
                return const SizedBox();
              },
            ),
          ),
        ),
        if (hasError)
          SizedBox(
            height: 20,
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                '$error',
                style: AppTextStyles.caption.caption.red,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          )
        else if (widget.additionalText?.isNotEmpty ?? false)
          SizedBox(
            height: 20,
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                '${widget.additionalText}',
                style: AppTextStyles.caption.caption.accent,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
      ],
    );
  }
}
