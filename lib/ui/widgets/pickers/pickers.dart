import 'dart:async';

import 'package:flutter/material.dart';

Future<int?> showYearPicker(
  BuildContext context, {
  int? initYear,
  void Function(int value)? onChanged,
}) =>
    showDialog<int?>(
      context: context,
      builder: (context) => Padding(
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * .15,
          horizontal: 16,
        ),
        child: Material(
          child: YearPicker(
            firstDate: DateTime(1980),
            lastDate: DateTime.now(),
            selectedDate: initYear == null ? DateTime.now() : DateTime(initYear),
            onChanged: Navigator.of(context).pop,
          ),
        ),
      ),
    );
