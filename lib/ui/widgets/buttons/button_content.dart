import 'package:flutter/material.dart';

import '../../../generated/assets.gen.dart';
import '../../theme/theme.dart';
import '../common/app_loading.dart';
import 'enums/button_enums.dart';

class AppButtonContent extends StatelessWidget {
  const AppButtonContent({
    Key? key,
    this.text,
    this.svgIcon,
    this.iconPosition = AppButtonIconPosition.left,
    this.isBusy = false,
    this.textColor,
    this.iconColor,
    this.buttonSize = AppButtonSize.big,
  })  : assert(
          text != null || svgIcon != null || isBusy,
          'text and svgIcon is null',
        ),
        super(key: key);

  final String? text;
  final SvgGenImage? svgIcon;
  final AppButtonIconPosition iconPosition;
  final bool isBusy;
  final Color? textColor;
  final Color? iconColor;
  final AppButtonSize buttonSize;

  @override
  Widget build(BuildContext context) {
    if (isBusy) {
      return AppLoading(color: iconColor);
    }
    final icon = svgIcon?.svg(
      color: iconColor,
      height: 12,
      width: 12,
    );
    final textStyle =
        (buttonSize == AppButtonSize.big ? AppTextStyles.button.m : AppTextStyles.button.s).copyWith(color: textColor);
    if (text == null && icon != null) {
      return icon;
    }
    if (text != null && icon == null) {
      return Text(
        '$text',
        style: textStyle,
        maxLines: 1,
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (iconPosition == AppButtonIconPosition.left)
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: icon,
            ),
          Text(
            '$text',
            style: textStyle,
            maxLines: 1,
          ),
          if (iconPosition == AppButtonIconPosition.right)
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: icon,
            ),
        ],
      );
    }
  }
}
