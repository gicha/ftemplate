import 'package:flutter/material.dart';

import 'app_button.dart';

class AppBottomButton extends StatelessWidget {
  const AppBottomButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.isBusy = false,
  }) : super(key: key);

  final String text;
  final VoidCallback? onPressed;
  final bool isBusy;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SafeArea(
        top: false,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Material(
                color: Colors.transparent,
                child: AppButton.primary(
                  text: text,
                  onPressed: onPressed,
                  isBusy: isBusy,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
