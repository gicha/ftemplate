import 'package:flutter/material.dart';

import '../../theme/theme.dart';
import 'app_common_button.dart';
import 'enums/button_enums.dart';
import 'models/button_color.dart';

class AppIconButton extends StatelessWidget {
  AppIconButton({
    Key? key,
    ButtonColor? color,
    required this.icon,
    required this.size,
    this.onPressed,
    this.withFill = false,
  }) : super(key: key) {
    this.color = color ??
        ButtonColor(
          active: AppColors.main.primary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        );
  }

  final IconData icon;
  late final ButtonColor color;
  final VoidCallback? onPressed;
  final bool withFill;
  final double size;

  @override
  Widget build(BuildContext context) {
    return AppCommonButton(
      onPressed: onPressed,
      buttonBuilder: (BuildContext context, AppButtonStatus status) {
        return AnimatedContainer(
          width: size,
          height: size,
          duration: const Duration(milliseconds: 200),
          decoration: BoxDecoration(
            color: withFill ? color.fromStatus(status) : null,
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Icon(
              icon,
              color: withFill ? AppColors.bg.primary : color.fromStatus(status),
              size: size,
            ),
          ),
        );
      },
    );
  }
}
