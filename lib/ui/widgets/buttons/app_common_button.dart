import 'package:flutter/material.dart';

import 'enums/button_enums.dart';

class AppCommonButton extends StatefulWidget {
  const AppCommonButton({
    Key? key,
    required this.buttonBuilder,
    this.onPressed,
  }) : super(key: key);

  final Widget Function(
    BuildContext context,
    AppButtonStatus status,
  ) buttonBuilder;
  final VoidCallback? onPressed;

  @override
  State<AppCommonButton> createState() => AppCommonStateButton();
}

class AppCommonStateButton extends State<AppCommonButton> {
  AppButtonStatus status = AppButtonStatus.idle;

  void initStatus() {
    if (status == AppButtonStatus.pressed) return;
    if (widget.onPressed == null) {
      status = AppButtonStatus.disabled;
    } else {
      status = AppButtonStatus.idle;
    }
  }

  void changeStatus(AppButtonStatus status) {
    this.status = status;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    initStatus();
    return GestureDetector(
      onTapDown: (_) => changeStatus(AppButtonStatus.pressed),
      onTapUp: (_) => changeStatus(AppButtonStatus.idle),
      onTapCancel: () => changeStatus(AppButtonStatus.idle),
      onTap: widget.onPressed,
      child: widget.buttonBuilder(context, status),
    );
  }
}
