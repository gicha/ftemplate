import 'package:flutter/material.dart';

import '../../../generated/assets.gen.dart';
import '../../theme/theme.dart';
import 'app_common_button.dart';
import 'button_content.dart';
import 'enums/button_enums.dart';
import 'models/button_color.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    Key? key,
    required this.onPressed,
    required this.textColor,
    required this.iconColor,
    this.text,
    this.svgIcon,
    this.iconPosition = AppButtonIconPosition.left,
    this.isBusy = false,
    this.color,
    this.withBorder = false,
    this.buttonSize = AppButtonSize.big,
  }) : super(key: key);

  factory AppButton.primary({
    Key? key,
    required VoidCallback? onPressed,
    String? text,
    SvgGenImage? svgIcon,
    AppButtonSize buttonSize = AppButtonSize.big,
    bool isBusy = false,
    AppButtonIconPosition iconPosition = AppButtonIconPosition.left,
  }) {
    final contentColor = ButtonColor(
      active: AppColors.text.staticWhite,
      pressed: AppColors.text.staticWhite,
      disable: AppColors.text.staticWhite,
    );
    return AppButton(
      key: key,
      onPressed: onPressed,
      text: text,
      svgIcon: svgIcon,
      buttonSize: buttonSize,
      isBusy: isBusy,
      iconPosition: iconPosition,
      color: ButtonColor(
        active: AppColors.main.primary,
        pressed: AppColors.main.primaryPress,
        disable: AppColors.main.primaryDisable,
      ),
      textColor: contentColor,
      iconColor: contentColor,
    );
  }

  factory AppButton.secondary({
    Key? key,
    required VoidCallback? onPressed,
    String? text,
    SvgGenImage? svgIcon,
    AppButtonSize buttonSize = AppButtonSize.big,
    bool isBusy = false,
    AppButtonIconPosition iconPosition = AppButtonIconPosition.left,
  }) =>
      AppButton(
        key: key,
        onPressed: onPressed,
        text: text,
        svgIcon: svgIcon,
        buttonSize: buttonSize,
        isBusy: isBusy,
        iconPosition: iconPosition,
        color: ButtonColor(
          active: AppColors.main.secondary,
          pressed: AppColors.main.secondaryPress,
          disable: AppColors.main.secondaryDisable,
        ),
        iconColor: ButtonColor(
          active: AppColors.stroke.iconPrimary,
          pressed: AppColors.stroke.iconPrimary,
          disable: AppColors.stroke.tertiaryLight,
        ),
        textColor: ButtonColor(
          active: AppColors.text.secondary,
          pressed: AppColors.text.secondary,
          disable: AppColors.text.tertiaryLight,
        ),
      );

  factory AppButton.white({
    Key? key,
    required VoidCallback? onPressed,
    String? text,
    SvgGenImage? svgIcon,
    AppButtonSize buttonSize = AppButtonSize.big,
    bool isBusy = false,
    AppButtonIconPosition iconPosition = AppButtonIconPosition.left,
  }) =>
      AppButton(
        key: key,
        onPressed: onPressed,
        text: text,
        svgIcon: svgIcon,
        buttonSize: buttonSize,
        isBusy: isBusy,
        iconPosition: iconPosition,
        color: ButtonColor(
          active: AppColors.text.staticWhite,
          pressed: AppColors.main.secondaryPress,
          disable: AppColors.main.secondaryDisable,
        ),
        iconColor: ButtonColor(
          active: AppColors.stroke.iconPrimary,
          pressed: AppColors.stroke.iconPrimary,
          disable: AppColors.stroke.tertiaryLight,
        ),
        textColor: ButtonColor(
          active: AppColors.text.secondary,
          pressed: AppColors.text.secondary,
          disable: AppColors.text.tertiaryLight,
        ),
      );

  factory AppButton.outlined({
    Key? key,
    required VoidCallback? onPressed,
    String? text,
    SvgGenImage? svgIcon,
    AppButtonSize buttonSize = AppButtonSize.big,
    bool isBusy = false,
    AppButtonIconPosition iconPosition = AppButtonIconPosition.left,
  }) =>
      AppButton(
        key: key,
        onPressed: onPressed,
        text: text,
        svgIcon: svgIcon,
        buttonSize: buttonSize,
        isBusy: isBusy,
        iconPosition: iconPosition,
        withBorder: true,
        iconColor: ButtonColor(
          active: AppColors.stroke.iconPrimary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        ),
        textColor: ButtonColor(
          active: AppColors.text.secondary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        ),
      );

  factory AppButton.ghost({
    Key? key,
    required VoidCallback? onPressed,
    String? text,
    SvgGenImage? svgIcon,
    AppButtonSize buttonSize = AppButtonSize.big,
    bool isBusy = false,
    AppButtonIconPosition iconPosition = AppButtonIconPosition.left,
  }) =>
      AppButton(
        key: key,
        onPressed: onPressed,
        text: text,
        svgIcon: svgIcon,
        buttonSize: buttonSize,
        isBusy: isBusy,
        iconPosition: iconPosition,
        color: ButtonColor(
          active: AppColors.bg.primary,
          pressed: AppColors.main.secondary,
          disable: AppColors.bg.primary,
        ),
        iconColor: ButtonColor(
          active: AppColors.stroke.iconPrimary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        ),
        textColor: ButtonColor(
          active: AppColors.text.secondary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        ),
      );

  final VoidCallback? onPressed;
  final String? text;
  final SvgGenImage? svgIcon;
  final AppButtonIconPosition iconPosition;
  final bool isBusy;
  final ButtonColor? color;
  final ButtonColor? textColor;
  final ButtonColor? iconColor;
  final bool withBorder;
  final AppButtonSize buttonSize;

  @override
  Widget build(BuildContext context) {
    return AppCommonButton(
      onPressed: isBusy ? () {} : onPressed,
      buttonBuilder: (
        BuildContext context,
        AppButtonStatus status,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: color?.fromStatus(status),
            borderRadius: AppBorderRadius.button,
            border: withBorder && iconColor != null
                ? Border.all(
                    color: iconColor!.fromStatus(status),
                  )
                : null,
          ),
          child: Padding(
            padding: buttonSize == AppButtonSize.big
                ? const EdgeInsets.symmetric(vertical: 8)
                : EdgeInsets.only(
                    bottom: 8,
                    top: 8,
                    left: svgIcon != null && iconPosition == AppButtonIconPosition.left ? 14 : 12,
                    right: svgIcon != null && iconPosition == AppButtonIconPosition.right ? 14 : 12,
                  ),
            child: SizedBox(
              width: buttonSize == AppButtonSize.big ? double.infinity : null,
              child: Center(
                child: AppButtonContent(
                  buttonSize: buttonSize,
                  iconColor: iconColor?.fromStatus(status),
                  textColor: textColor?.fromStatus(status),
                  iconPosition: iconPosition,
                  isBusy: isBusy,
                  svgIcon: svgIcon,
                  text: text,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
