import 'package:flutter/material.dart';

import '../enums/button_enums.dart';

class ButtonColor {
  ButtonColor({
    required this.active,
    required this.pressed,
    required this.disable,
  }) {
    _colorsMap = {
      AppButtonStatus.idle: active,
      AppButtonStatus.pressed: pressed,
      AppButtonStatus.disabled: disable,
    };
  }
  ButtonColor.fromColor(Color color)
      : active = color,
        pressed = color,
        disable = color,
        _colorsMap = {
          AppButtonStatus.idle: color,
          AppButtonStatus.pressed: color,
          AppButtonStatus.disabled: color,
        };
  final Color active;
  final Color pressed;
  final Color disable;

  late final Map<AppButtonStatus, Color> _colorsMap;

  Color fromStatus(AppButtonStatus status) => _colorsMap[status]!;
}
