import 'package:flutter/material.dart';

import '../../theme/theme.dart';
import 'app_common_button.dart';
import 'enums/button_enums.dart';
import 'models/button_color.dart';

class AppLinkButton extends StatelessWidget {
  const AppLinkButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.color = LinkColor.white,
  }) : super(key: key);

  final VoidCallback onPressed;
  final String text;
  final LinkColor color;

  @override
  Widget build(BuildContext context) {
    late ButtonColor textColor;
    if (color == LinkColor.white) {
      textColor = ButtonColor(
        active: AppColors.text.staticWhite,
        pressed: AppColors.text.staticWhite.withOpacity(0.5),
        disable: AppColors.text.staticWhite.withOpacity(0.5),
      );
    } else {
      textColor = ButtonColor(
        active: AppColors.main.primary,
        pressed: AppColors.main.primary.withOpacity(0.5),
        disable: AppColors.main.primary.withOpacity(0.5),
      );
    }

    return AppCommonButton(
      onPressed: onPressed,
      buttonBuilder: (context, status) {
        return Text(
          text,
          style: AppTextStyles.caption.caption.copyWith(color: textColor.fromStatus(status)),
        );
      },
    );
  }
}
