enum AppButtonStatus {
  idle,
  pressed,
  disabled,
}

enum AppButtonIconPosition {
  left,
  right,
}

enum AppButtonSize {
  big,
  small,
}

enum LinkColor {
  white,
  primary,
}
