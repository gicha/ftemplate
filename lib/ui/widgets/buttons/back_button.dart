import 'package:flutter/material.dart';

import '../../theme/theme.dart';
import 'icon_button.dart';
import 'models/button_color.dart';

class AppBackButton extends StatelessWidget {
  const AppBackButton({
    this.onPop,
    this.withBg = false,
    Key? key,
  }) : super(key: key);

  final VoidCallback? onPop;
  final bool withBg;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: withBg ? AppColors.bg.primary : null,
      ),
      child: AppIconButton(
        icon: Icons.arrow_back,
        size: 50,
        onPressed: onPop ?? Navigator.of(context).maybePop,
        color: ButtonColor(
          active: AppColors.main.primary,
          pressed: AppColors.main.primaryPress,
          disable: AppColors.main.primaryDisable,
        ),
      ),
    );
  }
}
