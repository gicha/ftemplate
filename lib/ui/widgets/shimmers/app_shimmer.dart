import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AppShimmer extends StatelessWidget {
  const AppShimmer({
    Key? key,
    required this.width,
    required this.heigth,
  }) : super(key: key);

  final double width;
  final double heigth;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: width,
        maxHeight: heigth,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Shimmer.fromColors(
          baseColor: const Color.fromARGB(255, 238, 238, 238),
          highlightColor: const Color.fromARGB(255, 212, 212, 212),
          child: Container(
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}
